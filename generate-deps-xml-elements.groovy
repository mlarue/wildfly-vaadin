project.properties['module.resources'] = new File(project.build.directory + '/dependency')
  .listFiles()
  .sort()
  .collect {'        <resource-root path="' + it.name + '" />'}
  .join('\n')
